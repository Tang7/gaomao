# Program Starts with Fibonacci

[Fibonacci number](https://en.wikipedia.org/wiki/Fibonacci_number) is an interesting finding in mathematics, but it is also a good example to show/gain efficient logic thinking in programming.

## Loop Solution
Simply follow the Fibonacci theory and create a while loop:
```cpp
int fibonacci(int n) {
    int fn_2, fn_1 = 0, 1;
    if (fn_2 >= n) return f0;
    if (fn_1 == n) return f1;
    
    int fn = fn_1 + fn_2;
    while (fn <= n) {
        fn_2 = fn_1;
        fn_1 = fn;
        fn = fn_1 + fn_2;
    }
    return fn;
}
```
When I studied programming, I was thinking why we have to write so many code to achieve a simple math equation.
> But in programming side, this is an efficient solution: 
1. Memory space: O(1)
2. Time cost: O(n)

## Recursive Solution
My first thought about recursive is, we could solve above handy job with less code. So here is a naive revursive solution for Fibonacci:
```python
def fibonacci(n):
    if n < 0: return 0
    if n < 2: return n
    return fibonacci(n-1) + fibonacci(n-2)
```
Unfortunately, this is a quite bad solution. The running time is quite large even if we input 40.

> To inspect the issue, when we input 40, the first recursive clause would call fibonacci(38) and fibonacci(39). So that:
> fibonacci(38) would call fibonacci(36) and fibonacci(37)
> fibonacci(39) would call fibonacci(38) and fibonacci(37)

No only the function would call duplicate function many times, **the most important drawback is the program grows expontially**: every function call another two same efficient functions, and so as their sub-calls.
    1. Memory space: O(2^n)
    2. Time cost: O(2^n)

Actually, recursive is a powerful feature in programming, and it could solve complicate problem in an easier way (the recursive way). This example only shows that we should not abuse of recursive.

## Go Solution
Like C++ and Python, Go also has an iterative solution for Fibonacci number, but Go has its *Go way*:
> [**Closures**](https://tour.golang.org/moretypes/25)

```go
func fibonacci() func() int {
	a, b := 0, 1
	return func () int {
		b, a = a, a+b
		return b
	}
}
```
To find certain Fibonacci number, we only need to run this function repeatly:
```go
f := fibonacci()
for i:= 0; i < 20; i++ {
    fmt.Println(f())
}
```