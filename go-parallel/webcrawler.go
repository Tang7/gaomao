package main

import (
	"errors"
	"fmt"
	"sync"
	"time"
)

// Fetcher :Fetch returns the body of URL and
// a slice of URLs found on that page.
type Fetcher interface {
	Fetch(url string) (body string, urls []string, err error)
}

// fakeFetcher is Fetcher that returns canned results.
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
	body string
	urls []string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}
	return "", nil, fmt.Errorf("not found: %s", url)
}

// fetchCache store the fetced urls, no empty error indicate new url
type fetchCache struct {
	urlResults map[string]error
	sync.Mutex
}

var fetchResults = fetchCache{urlResults: make(map[string]error)}

var errorDuplicate = errors.New("duplicate url")

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func Crawl(url string, depth int, fetcher Fetcher) {
	if depth <= 0 {
		return
	}
	// fmt.Println("Start Crawling URL ", url)

	fetchResults.Lock()
	if _, err := fetchResults.urlResults[url]; err {
		fetchResults.Unlock()
		return
	}
	fetchResults.urlResults[url] = errorDuplicate
	fetchResults.Unlock()

	_, urls, err := fetcher.Fetch(url)
	fetchResults.Lock()
	fetchResults.urlResults[url] = err
	fetchResults.Unlock()

	if err != nil {
		fmt.Println(err)
		return
	}
	// fmt.Printf("found: %s %q\n", url, body)

	crawled := make(chan string)
	for _, u := range urls {
		go func(url string) {
			Crawl(url, depth-1, fetcher)
			crawled <- url
		}(u)
		// <-crawled
	}

	// fmt.Println("Crawled URL ", <-crawled, "Current goroutine ", url)

	for range urls {
		// fmt.Println("Crawler URL ", <-crawled, " Current url ", url, " Sub Url", u)
		<-crawled
	}

	// fmt.Println("Done URL ", url)
	return
}

func main() {
	defer timeTrack(time.Now())
	Crawl("https://golang.org/", 4, fetcher)
	for v, err := range fetchResults.urlResults {
		fmt.Println(v, err)
	}
}

func timeTrack(start time.Time) {
	elapse := time.Since(start)
	fmt.Println("Running time ", elapse)
}

// fetcher is a populated fakeFetcher.
var fetcher = fakeFetcher{
	"https://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"https://golang.org/pkg/",
			"https://golang.org/cmd/",
		},
	},
	"https://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"https://golang.org/",
			"https://golang.org/cmd/",
			"https://golang.org/pkg/fmt/",
			"https://golang.org/pkg/os/",
		},
	},
	"https://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
	"https://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
}
