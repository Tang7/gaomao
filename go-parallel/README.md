# Go Parallel

Using example "webcrawler.go" from [A Tour of Go ](https://tour.golang.org/concurrency/10) to learn the Concurrency and Parallel feature of Go!

## The Problem

Try to fetch URLs and its sub-URLs in parallel, and non-repeat.

    "https://golang.org/": {"The Go Programming Language",
                           ["https://golang.org/pkg/",
			               "https://golang.org/cmd/"]}

    "https://golang.org/pkg/": {"Packages",
                               ["https://golang.org/",
			                   "https://golang.org/cmd/",
                               "https://golang.org/pkg/fmt/",
                               "https://golang.org/pkg/os/"]}

    "https://golang.org/pkg/fmt/": {"Package fmt",
                                   ["https://golang.org/pkg/",
			                       "https://golang.org/cmd/"]}

    "https://golang.org/pkg/os/": {"Package os",
                                  ["https://golang.org/",
			                      "https://golang.org/pkg/"]}

For each URL, the data structure provides its name string and its sub-URLs list. 

> The crawl logic is when we search for any URL,  
> Check whether the URL present as the KEY in the data structure. 
> And we continue to found sub-URLs if the URL present. 

----
## Step 1: Non-Repeat

> Create a cache map to store the crawled URL
* Display an error msg when try to search a duplicate URL
``` go    
type fetchCache struct {
     urlResults map[string]error
}
var fetchResults = fetchCache{urlResults: make(map[string]error)}
var errorDuplicate = errors.New("duplicate url")
```
* Early return when duplicate URL found
``` go
if _, err := fetchResults.urlResults[url]; err {
	return
}
fetchResults.urlResults[url] = errorDuplicate
```

Check the results:
```sh
found: https://golang.org/ "The Go Programming Language"
found: https://golang.org/pkg/ "Packages"
not found: https://golang.org/cmd/
found: https://golang.org/pkg/fmt/ "Package fmt"
found: https://golang.org/pkg/os/ "Package os"
not found: https://golang.org/cmd/
Running time  67.872µs
```
1. Every avalible URL is found, and no duplicate appears;
2. Same "Not Found" error for the same URL -- the drawback of no parallel.

## Step 2: Try Parallel
From step 1, we know we try to find non-exist url twice due to the program runs sequentially, crawl https://golang.org/pkg/ and then crawl https://golang.org/pkg/os/. 

> Goroutine for sub-URLs
```go
	crawled := make(chan string)
	for _, u := range urls {
		go func(url string) {
			Crawl(url, depth-1, fetcher)
			crawled <- url
		}(u)
	}

	fmt.Println("Crawled URL ", <-crawled)
```
Crawl the first URL and its sub-URLs instead of all URLs. 
```sh
Start Crawling URL  https://golang.org/
found: https://golang.org/ "The Go Programming Language"
Start Crawling URL  https://golang.org/cmd/
not found: https://golang.org/cmd/
Crawled URL  https://golang.org/cmd/
Start Crawling URL  https://golang.org/pkg/
found: https://golang.org/pkg/ "Packages"
Running time  101.76µs
```

The interesting thing is **only one "Crawled URL" shows**.
Check current goroutine print "Crawled URL":
```sh
Crawled URL  https://golang.org/cmd/ 
Current goroutine  https://golang.org/
```
> Channel connect concurrent goroutines only when both sender and receiver are ready
> Only goroutines "https://golang.org/" and "https://golang.org/cmd/" are connected
> ```Crawl(cmd/) ```is early return, result in ```crawl<-"cmd/" ``` corresponding to ```<-crawl ``` in ```".org/" ``` goroutine.
> While ```Crawl(pkg/) ``` is blocked due to no sender responds to ```<-crawl``` in ```pkg/``` goroutine.

More importantly, current implementation is not stable, racing would happen if multiple goroutines starts at same time, and the results vary. But the key finding for this implementation is:
1. The channel receiver and sender pair is imbalance.
2. Fetching duplicate URLs happens due to wrong parallel implementation.

## Step 3: Fix imbalance receiver and sender
In first for loop, we always create ```range urls``` senders, thus we need call the same number of receivers as well:
```go
    for range urls {
        <-crawl
    }
```
By searching for "not found" case, only one "not found: https://golang.org/cmd/" display. We could make sure the goroutines are run parallel.


## Step 4: Finalize
Lets print the cache result we get:
```sh
https://golang.org/ duplicate url
https://golang.org/cmd/ duplicate url
https://golang.org/pkg/ duplicate url
https://golang.org/pkg/fmt/ duplicate url
https://golang.org/pkg/os/ duplicate url
```
Although we finish the targets defined in step 1, but the actual results are broken. Since we only set ```fetchResults.urlResults[url] = errorDuplicate```.
However, the actual results are obtained from function ```Fetch(url)```, which is ```err``` in our case.
At last, change the correct fetch result:
```go
    body, urls, err := fetcher.Fetch(url)
	fetchResults.Lock()
	fetchResults.urlResults[url] = err
	fetchResults.Unlock()
```
Finaly we fetch the results in prallel and non-repeat.
```sh
https://golang.org/pkg/os/ <nil>
https://golang.org/ <nil>
https://golang.org/cmd/ not found: https://golang.org/cmd/
https://golang.org/pkg/ <nil>
https://golang.org/pkg/fmt/ <nil>
Running time  112.931µs
```

----
## Side Note
Although the task is finished in parallel and non-repeat, the running time is slightly larger than sequantial implementation. Since this job is too small to fit scalable implementation, where go is not really shine much.